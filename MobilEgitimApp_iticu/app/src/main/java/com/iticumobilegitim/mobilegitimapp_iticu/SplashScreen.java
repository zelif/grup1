package com.iticumobilegitim.mobilegitimapp_iticu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.iticumobilegitim.mobilegitimapp_iticu.activity._IndexActivity;

/**
 * Created by Hilal on 29.05.2016.
 */
public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(5000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    Intent intent = new Intent(SplashScreen.this,_IndexActivity.class);
                    startActivity(intent);
                }
            }
        };
        timerThread.start();
    }





    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}
