package com.iticumobilegitim.mobilegitimapp_iticu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.iticumobilegitim.mobilegitimapp_iticu.R;

public class DetailActivityForBeginner extends AppCompatActivity {

    private static final String BUNDLE_EXTRAS = "BUNDLE_EXTRAS";
    private static final String TITLE_BEGINNER = "TITLE_BEGINNER";
    private static final String DETAIL_BEGINNER = "DETAIL_BEGINNER";
    private static final String DESCRIPTION_BEGINNER = "DESCRIPTION_BEGINNER";
    private static final String PATH_BEGINNER = "PATH_BEGINNER";
    private static final int REQUEST_CODE_BEGINNER = 101;


    private TextView tvDetail;
    private Button btnShowPdf;

    private String detailBeginner;
    private String titleBeginner;
    private String descriptionBeginner;
    private String pathBeginner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_activity_for_beginner);

        tvDetail = (TextView) findViewById(R.id.tv_detail);
        btnShowPdf = (Button) findViewById(R.id.btn_show_pdf);


        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Bundle extras = getIntent().getBundleExtra(BUNDLE_EXTRAS);
        detailBeginner = extras.getString(DETAIL_BEGINNER);
        titleBeginner = extras.getString(TITLE_BEGINNER);
        descriptionBeginner = extras.getString(DESCRIPTION_BEGINNER);
        pathBeginner = extras.getString(PATH_BEGINNER);

        tvDetail.setText(detailBeginner);
        getSupportActionBar().setTitle(titleBeginner);
        getSupportActionBar().setSubtitle(descriptionBeginner);


        btnShowPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PdfViewForBeginner.class);
                Bundle extras = new Bundle();
                extras.putString(PATH_BEGINNER,pathBeginner);
                extras.putString(DESCRIPTION_BEGINNER, descriptionBeginner);
                intent.putExtra(BUNDLE_EXTRAS, extras);
                startActivity(intent);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
