package com.iticumobilegitim.mobilegitimapp_iticu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.iticumobilegitim.mobilegitimapp_iticu.R;

public class DetailActivityForIntermediate extends AppCompatActivity {

    private static final String BUNDLE_EXTRAS = "BUNDLE_EXTRAS";
    private static final String TITLE_INTERMEDIATE = "TITLE_INTERMEDIATE";
    private static final String DETAIL_INTERMEDIATE = "DETAIL_INTERMEDIATE";
    private static final String DESCRIPTION_INTERMEDIATE = "DESCRIPTION_INTERMEDIATE";
    private static final String PATH_INTERMEDIATE = "PATH_INTERMEDIATE";

    private static final int REQUEST_CODE_INTERMEDIATE = 102;


    private TextView tvDetail;
    private Button btnShowPdf;

    private String detailIntermediate;
    private String titleIntermediate;
    private String descriptionIntermediate;
    private String pathIntermediate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_activity_for_intermediate);

        tvDetail = (TextView) findViewById(R.id.tv_detail_i);
        btnShowPdf = (Button) findViewById(R.id.btn_show_pdf_i);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Bundle extras = getIntent().getBundleExtra(BUNDLE_EXTRAS);
        detailIntermediate = extras.getString(DETAIL_INTERMEDIATE);
        titleIntermediate = extras.getString(TITLE_INTERMEDIATE);
        descriptionIntermediate = extras.getString(DESCRIPTION_INTERMEDIATE);
        pathIntermediate = extras.getString(PATH_INTERMEDIATE);

        tvDetail.setText(detailIntermediate);
        getSupportActionBar().setTitle(titleIntermediate);
        getSupportActionBar().setSubtitle(descriptionIntermediate);

        btnShowPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), PdfViewForIntermediate.class);
                Bundle extras = new Bundle();
                extras.putString(PATH_INTERMEDIATE,pathIntermediate);
                extras.putString(DESCRIPTION_INTERMEDIATE, descriptionIntermediate);
                intent.putExtra(BUNDLE_EXTRAS, extras);
                startActivity(intent);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            //            case R.id.about_info:
//                Intent intentAbout = new Intent(getApplicationContext(), AboutActivity.class);
//                startActivity(intentAbout);
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}

