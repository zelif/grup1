package com.iticumobilegitim.mobilegitimapp_iticu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.iticumobilegitim.mobilegitimapp_iticu.R;
import com.iticumobilegitim.mobilegitimapp_iticu.model.Secur;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class PdfViewForBeginner extends AppCompatActivity implements OnLoadCompleteListener,OnPageChangeListener,OnErrorListener {
    private static final String BUNDLE_EXTRAS = "BUNDLE_EXTRAS";
    private static final String DESCRIPTION_BEGINNER = "DESCRIPTION_BEGINNER";
    private static final String PATH_BEGINNER = "PATH_BEGINNER";

    private PDFView pdfView;

    private String descriptionBeginner;
    private String pathBeginner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_view_beginner);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        Bundle extras = getIntent().getBundleExtra(BUNDLE_EXTRAS);
        descriptionBeginner = extras.getString(DESCRIPTION_BEGINNER);
        pathBeginner = extras.getString(PATH_BEGINNER);

        getSupportActionBar().setTitle(descriptionBeginner);

       pdfView = (PDFView) findViewById(R.id.pdfView_beginner);

        try {
            InputStream is = getAssets().open(pathBeginner);
            Secur.decode(getApplicationContext(),is , pathBeginner);

            File wwRoot = new File(getApplicationContext().getFilesDir()+"/decrypted.pdf");

            pdfView.fromFile(wwRoot)
                    .defaultPage(1)
                    .showMinimap(false)
                    .enableSwipe(true)
                    .onLoad(this)
                    .onPageChange(this)
                    .onError(this)
                    .load();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onError(Throwable t) {

    }

    @Override
    public void loadComplete(int nbPages) {

    }

    @Override
    public void onPageChanged(int page, int pageCount) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
//            case R.id.about_info:
//                Intent intentAbout = new Intent(getApplicationContext(), AboutActivity.class);
//                startActivity(intentAbout);
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
