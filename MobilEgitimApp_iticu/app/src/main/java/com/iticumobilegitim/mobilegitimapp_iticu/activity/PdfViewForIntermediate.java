package com.iticumobilegitim.mobilegitimapp_iticu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.iticumobilegitim.mobilegitimapp_iticu.R;
import com.iticumobilegitim.mobilegitimapp_iticu.model.Secur;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class PdfViewForIntermediate extends AppCompatActivity implements OnLoadCompleteListener, OnPageChangeListener, OnErrorListener {

    private static final String BUNDLE_EXTRAS = "BUNDLE_EXTRAS";
    private static final String DESCRIPTION_INTERMEDIATE = "DESCRIPTION_INTERMEDIATE";
    private static final String PATH_INTERMEDIATE = "PATH_INTERMEDIATE";

    private PDFView pdfView;

    private String descriptionIntermediate;
    private String pathIntermediate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_view_intermediate);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        Bundle extras = getIntent().getBundleExtra(BUNDLE_EXTRAS);
        descriptionIntermediate = extras.getString(DESCRIPTION_INTERMEDIATE);
        pathIntermediate = extras.getString(PATH_INTERMEDIATE);

        getSupportActionBar().setTitle(descriptionIntermediate);

        pdfView = (PDFView) findViewById(R.id.pdfView_intermediate);


        try {
            InputStream is = getAssets().open(pathIntermediate);
            Secur.decode(getApplicationContext(),is , pathIntermediate);

            File wwRoot = new File(getApplicationContext().getFilesDir()+"/decrypted.pdf");

            pdfView.fromFile(wwRoot)
                    .defaultPage(1)
                    .showMinimap(false)
                    .enableSwipe(true)
                    .onLoad(this)
                    .onPageChange(this)
                    .onError(this)
                    .load();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Throwable t) {

    }

    @Override
    public void loadComplete(int nbPages) {

    }

    @Override
    public void onPageChanged(int page, int pageCount) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            //            case R.id.about_info:
//                Intent intentAbout = new Intent(getApplicationContext(), AboutActivity.class);
//                startActivity(intentAbout);
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
