package com.iticumobilegitim.mobilegitimapp_iticu.activity;

import android.app.Fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.iticumobilegitim.mobilegitimapp_iticu.MainActivity;
import com.iticumobilegitim.mobilegitimapp_iticu.R;
import com.iticumobilegitim.mobilegitimapp_iticu.data.AboutData;


public class _IndexActivity extends AppCompatActivity {


    Button trainings, btnRate, btnEmail;
    TextView about;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index_a);

        trainings = (Button) findViewById(R.id.btn_trainings);
        btnRate = (Button) findViewById(R.id.rate);
        btnEmail = (Button) findViewById(R.id.email);
        about = (TextView) findViewById(R.id.about);

        trainings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTrainings = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intentTrainings);
            }
        });

        about.setText(AboutData.getAboutInfo());
        btnRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateProject();
            }
        });

        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                composeEmail();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.about_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            case R.id.share:
                shareProject();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void shareProject() {
//        Intent sendIntent = new Intent();
//        sendIntent.setAction(Intent.ACTION_SEND);
//        sendIntent.putExtra(Intent.EXTRA_TEXT,"Java-Android derslerini içeren bu uygulamayı indirebilirsiniz:");
//        sendIntent.setType("text/plain");
//        startActivity(sendIntent);
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Java-Android derslerini içeren bu uygulamayı indirebilirsiniz:";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "EDUAPP Uygulamasını İndir!");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Uygulamayı paylaş:"));
    }

    private void rateProject() {

        try {
            Uri uri = Uri.parse("market://details?id=" + getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id="
                            + getPackageName())));
        }
    }

    public void composeEmail() {
        String[] address = {"projeofisi@ticaret.edu.tr"};
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        intent.putExtra(Intent.EXTRA_SUBJECT, "EDUAPP Uygulaması Hakkında");
        intent.putExtra(Intent.EXTRA_EMAIL, address);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(intent, "Send Email"));
        }
    }
}
