package com.iticumobilegitim.mobilegitimapp_iticu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iticumobilegitim.mobilegitimapp_iticu.R;
import com.iticumobilegitim.mobilegitimapp_iticu.listener.ItemClickCallback;
import com.iticumobilegitim.mobilegitimapp_iticu.model.Beginner;

import java.util.List;

/**
 * Created by saliha on 4.6.2016.
 */
public class BeginnerAdapter extends RecyclerView.Adapter<BeginnerAdapter.ViewHolder> {

    private List<Beginner> mBeginner;
    private final Context mContext;
    private final ItemClickCallback itemClickCallback;

    public BeginnerAdapter(ItemClickCallback itemClickCallback,List<Beginner> mBeginner, Context mContext) {
        this.mBeginner = mBeginner;
        this.mContext = mContext;
        this.itemClickCallback = itemClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View rowView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_row_beginner, parent, false);
        ViewHolder viewHolder = new ViewHolder(rowView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Beginner selectedTopic = mBeginner.get(position);
        holder.topicTitle.setText(selectedTopic.getTopicTitle());
        holder.topicDescription.setText(selectedTopic.getTopicDescription());
    }

    @Override
    public int getItemCount() {

        if (mBeginner != null) {
            return mBeginner.size();
        } else {
            return 0;
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public View container;
        public ImageView nextArrow;
        public TextView topicTitle;
        public TextView topicDescription;

        public ViewHolder(View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.cont_item_beginner);
            nextArrow = (ImageView) itemView.findViewById(R.id.next_arrow);
            topicTitle = (TextView) itemView.findViewById(R.id.topic_title);
            topicDescription = (TextView) itemView.findViewById(R.id.topic_description);
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (v.getId() == R.id.cont_item_beginner){
                itemClickCallback.onItemClick(getAdapterPosition());
            }

        }
    }
}