package com.iticumobilegitim.mobilegitimapp_iticu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iticumobilegitim.mobilegitimapp_iticu.R;
import com.iticumobilegitim.mobilegitimapp_iticu.listener.ItemClickCallback;
import com.iticumobilegitim.mobilegitimapp_iticu.model.Intermediate;

import java.util.List;

/**
 * Created by saliha on 4.6.2016.
 */
public class IntermediateAdapter extends RecyclerView.Adapter<IntermediateAdapter.ViewHolder> {

    private List<Intermediate> mIntermediate;
    private final Context mContext;
    private final ItemClickCallback itemClickCallback;

    public IntermediateAdapter(ItemClickCallback itemClickCallback,List<Intermediate> mIntermediate, Context mContext) {
        this.mIntermediate = mIntermediate;
        this.mContext = mContext;
        this.itemClickCallback = itemClickCallback;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_row_intermediate, parent, false);
        ViewHolder viewHolder = new ViewHolder(rowView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Intermediate selectedTopic = mIntermediate.get(position);
        holder.topicTitle_i.setText(selectedTopic.getTopicTitle());
        holder.topicDescription_i.setText(selectedTopic.getTopicDescription());

    }

    @Override
    public int getItemCount() {
        if (mIntermediate != null) {
            return mIntermediate.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public View container_i;
        public ImageView nextArrow_i;
        public TextView topicTitle_i;
        public TextView topicDescription_i;

        public ViewHolder(View itemView) {
            super(itemView);
            nextArrow_i = (ImageView) itemView.findViewById(R.id.next_arrow_intermediate);
            topicTitle_i = (TextView) itemView.findViewById(R.id.topic_title_intermediate);
            topicDescription_i = (TextView) itemView.findViewById(R.id.topic_description_intermediate);
            container_i= itemView.findViewById(R.id.cont_item_intermediate);
            container_i.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.cont_item_intermediate){
                itemClickCallback.onItemClick(getAdapterPosition());
            }

        }
    }
}
