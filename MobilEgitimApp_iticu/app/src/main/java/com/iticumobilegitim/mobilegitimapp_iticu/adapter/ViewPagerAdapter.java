package com.iticumobilegitim.mobilegitimapp_iticu.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.iticumobilegitim.mobilegitimapp_iticu.fragment.BeginnerFragment;
import com.iticumobilegitim.mobilegitimapp_iticu.fragment.IntermediateFragment;

/**
 * Created by User on 4.6.2016.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment selectedFragment;
        switch (position){
            case 0:
                selectedFragment = new BeginnerFragment();
                break;
            case 1:
                selectedFragment = new IntermediateFragment();
                break;
            default:
                selectedFragment = new BeginnerFragment();

        }
        return selectedFragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        String title = "";
        switch (position){
            case 0:
                title = "Başlangıç";
                break;
            case 1:
                title = "Orta";
                break;
        }
        return title;
    }
}
