package com.iticumobilegitim.mobilegitimapp_iticu.data;

import com.iticumobilegitim.mobilegitimapp_iticu.model.About;

/**
 * Created by saliha on 2.6.2016.
 */
public class AboutData {

    public static String getAboutInfo() {


        About about = new About();
        about.setAboutText("“Mobil Çözümlerin Adresi: Kadın” projesi, İstanbul Kalkınma Ajansı tarafından kar amacı gütmeyen kuruluşlara yönelik açılan 2015 yılı “Kadın İstihdamın Arttırılması Mali Destek Programı” kapsamında hibe desteği almaya hak kazanmıştır.\n" +
                "\n" +
                "İstanbul Ticaret Odası liderliğinde İstanbul Ticaret Üniversitesi ortaklığında ve TOBB İstanbul Kadın Girişimciler Kurulu desteği ile yürütülmekte olan projede Teknopark İstanbul A.Ş. ve Kadın ve Demokrasi Derneği (KADEM) iştirakçi olarak yer almaktadır.\n" +
                "\n" +
                "Toplumda kadına biçilen geleneksel roller, kadını iş gücü konusunda dezavantajlı bir duruma sokmaktadır. Bu durumun sonucunda, kadın istihdamına yönelik kanallar olumsuz yönde etkilenmektedir. İstihdam alanında dezavantajlı gruplarda yer alan kadınların, işe giriş veya çalışma hayatına devam etme konularında, iş gücü piyasalarına entegrasyonları daha düşüktür. Bu bağlamda projenin temel gayesi kadınların istihdam kapasitesini ve kadın girişimciliğini arttırmaktır.\n" +
                "\n" +
                "Projenin hedef grubunu, en az üniversite öğrencisi veya mezunu olan, mobil teknoloji endüstrisinde çalışmaya, mobil teknolojiler alanındaki iş fikrini hayata geçirmeye istekli 40 kadın ve aynı kapsamdaki eğitimlere uzaktan eğitim sistemi vasıtasıyla katılacak 60 kadın oluşturmaktadır. Projenin nihai yararlanıcıları; eğitim programlarına katılacak kadınların çalışacakları işletmeler, kadınların girişimci olarak iş kurması ile bu işletmelerde çalışacak kişiler, kadınların sosyal çevreleri ve mobil hizmetler alanında faaliyet gösteren işletmeler oluşturmaktadır.\n" +
                "\n" +
                "Mobil Çözümlerin Adresi: Kadın Projesi ile İstanbul'daki kadınların mobil teknoloji sektöründe iş gücüne ve istihdama yönelik faaliyetlerinin arttırılması yoluyla kadınların ekonomik anlamda güçlenmesi, ülke ekonomilerine katkıları ve bu hizmetlerde etkin rol üstlenmesi hedeflenmektedir.\n");

        return about.getAboutText();
    }

}
