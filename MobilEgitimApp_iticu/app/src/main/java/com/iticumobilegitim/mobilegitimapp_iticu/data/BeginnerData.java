package com.iticumobilegitim.mobilegitimapp_iticu.data;

import com.iticumobilegitim.mobilegitimapp_iticu.model.Beginner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created saliha on 2.6.2016.
 */
public class BeginnerData {

    public static List<Beginner> getBeginnerTopics() {
        List<Beginner> beginners = new ArrayList<>();



        Beginner beginner = new Beginner();
        beginner.setId(1);
        beginner.setTopicTitle("Konu 1 ");
        beginner.setTopicDescription("Genel Java Dili");
        beginner.setTopicDetail("a.\tProgramlama Nedir?\n" +
                "b.\tDerleyici ve Yorumlayıcı Kavramları\n" +
                "c.\tJava Programlama Dilinin Özellikleri\n" +
                "d.\tJava Programlama Dilinin Yapısı\n" +
                "e.\tJava Programlama Dilinin Kalıbı\n" +
                "f.\tAlgoritma Nedir?\t\n" +
                "g.\tProgramlama Dillerinin Temel Yapı Taşları\n" +
                "h.\tDeğişkenler\n" +
                "i.\tVeri Tipleri\n" +
                "j.\tOperatörler \n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(2);
        beginner.setTopicTitle("Konu 2");
        beginner.setTopicDescription("Programlama ve Karar Yapıları");
        beginner.setTopicDetail("a.\tProgramlama\n" +
                "b.\tEnum Veri Tipi\n" +
                "c.\tSeçimli-Karar Yapıları\n");
        beginners.add(beginner);


        beginner = new Beginner();
        beginner.setId(3);
        beginner.setTopicTitle("Konu 3");
        beginner.setTopicDescription("Tekrarlı Yapılar ve Giriş-Çıkış İşlemleri");
        beginner.setTopicDetail("a.\tDöngüler (Loops)/Tekrarlı Yapılar\n" +
                "b.\tGiriş-Çıkış (İO) İşlemleri\n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(4);
        beginner.setTopicTitle("Konu 4");
        beginner.setTopicDescription("Continue-Break Kavramları, İç İçe Döngüler ve Matematiksel Fonksiyonlar");
        beginner.setTopicDetail("a.\tContinue Deyimi\n" +
                "b.\tBreak Deyimi\n" +
                "c.\tİç İçe Döngüler\n" +
                "d.\tMatematiksel Fonksiyonlar\n" +
                "e.\tRastgele Sayı Üretimi\n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(5);
        beginner.setTopicTitle("Konu 5");
        beginner.setTopicDescription("Diziler (Arrays)");
        beginner.setTopicDetail("a.\tDizi Kavramı\n" +
                "b.\tTek Boyutlu Diziler\n" +
                "c.\tDizi Elemanlarına Değer Aktarımı\n" +
                "d.\tDizi Elemanlarını Sıralama\n" +
                "e.\tİki Boyutlu Diziler (Matrisler)\n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(6);
        beginner.setTopicTitle("Konu 6");
        beginner.setTopicDescription("Altprogram ve Fonksiyonlar");
        beginner.setTopicDetail("a.\tFonksiyon (Function)\n" +
                "b.\tÖzyineleme (Recursive Function)\n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(7);
        beginner.setTopicTitle("Konu 7");
        beginner.setTopicDescription("Dosya İşlemleri");
        beginner.setTopicDetail("");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(8);
        beginner.setTopicTitle("Konu 8");
        beginner.setTopicDescription("Hata Yakalama (Exception Handling)");
        beginner.setTopicDetail("");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(9);
        beginner.setTopicTitle("Konu 9");
        beginner.setTopicDescription("Veri Yapıları, Yığın ve Kuyruk");
        beginner.setTopicDetail("a.\tYığın Yapısı\n" +
                "b.\tKuyruk Yapısı\n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(10);
        beginner.setTopicTitle("Konu 10");
        beginner.setTopicDescription("Bağlı Liste");
        beginner.setTopicDetail("");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(11);
        beginner.setTopicTitle("Konu 11");
        beginner.setTopicDescription("Nesne Yönelimli Programlama ve Temel Kavramlar");
        beginner.setTopicDetail("a.\tUML Diyagramları\n" +
                "b.\tNesne Tabanlı Tasarım\n" +
                "c.\tFonksiyonlar (Methodlar)\n" +
                "d.\tKalıtım (Inheritence) Mantığı\n" +
                "e.\tÜzerine Yazma (Method Overriding)\n" +
                "f.\tSarma (Encapsulation)\n" +
                "g.\tÇok Biçimlilik (Polimorfizm)\n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(12);
        beginner.setTopicTitle("Konu 12");
        beginner.setTopicDescription("Android'e Giriş");
        beginner.setTopicDetail("a.\tAndroid Kavramı\n" +
                "b.\tAndroid Geliştirme Araçları\n" +
                "c.\tAndroid Geliştirme Ortamı Kurulumu\n" +
                "d.\tAndroid SDK Yöneticisi\n" +
                "e.\tEmulatör Kullanımı\n" +
                "f.\tUygulama Temelleri\n" +
                "g.\tCihaz Uyumlulukları\n" +
                "h.\tSistem İzinleri\n" +
                "i.\tAndroid Studio Hakkında\n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(13);
        beginner.setTopicTitle("Konu 13");
        beginner.setTopicDescription("Uygulama Elemanları, Intentler");
        beginner.setTopicDetail("a.\tIntent ve Intent Filtreleri\n" +
                "b.\tYaygın İntent’ler\n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(14);
        beginner.setTopicTitle("Konu 14");
        beginner.setTopicDescription("Aktiviteler, Fragmentlar");
        beginner.setTopicDetail("a.\tAktiviteler\n" +
                "i.\tAktivite Oluşturmak\n" +
                "ii.\tSonuç Beklenen Aktivite Başlatmak\n" +
                "iii.\tAktivite Hayat Döngüsü\n" +
                "iv.\tYeni Aktiviteye Veri Aktarımı\t\n" +
                "b.\tFragmentlar\n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(15);
        beginner.setTopicTitle("Konu 15");
        beginner.setTopicDescription("Fragmentlar, Servisler, İçerik Sağlayıcılar, İşlemler");
        beginner.setTopicDetail("a.\tFragmentların Yönetilmesi \n" +
                "b.\tGörevler ve Uygulama Yığını\n" +
                "c.\tServisler\n" +
                "d.\tİçerik Sağlayıcılar\n" +
                "e.\tİşlemler ve Thread’ler\n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(16);
        beginner.setTopicTitle("Konu 16");
        beginner.setTopicDescription("Android Debug Bridge(adb) Aracı ve .apk Dosyası Çıkarma");
        beginner.setTopicDetail("a.\tUygulama Hata Ayıklama\n" +
                "b.\tUygulama Log Yazdırma\n" +
                "c.\tUygulamanın .Apk Dosyasının Çıkarılması\n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(17);
        beginner.setTopicTitle("Konu 17");
        beginner.setTopicDescription("Uygulama Kaynakları");
        beginner.setTopicDetail("a.\tTemel Bilgiler\n" +
                "b.\tKaynakların Sağlanması\n" +
                "c.\tKaynaklara Erişim\n" +
                "d.\tKaynakların Çalışma Zamanında Güncellenmesi\n" +
                "e.\tYerelleştirme\n" +
                "f.\tKaynak Tipleri\n" +
                "i.\tAnimasyon\n" +
                "ii.\tRenk Durum Listesi\n" +
                "iii.\tÇizilebilir\n" +
                "iv.\tDüzen\n" +
                "v.\tMenü\n" +
                "vi.\tString\n" +
                "vii.\tStil\n");
        beginners.add(beginner);

        beginner = new Beginner();
        beginner.setId(18);
        beginner.setTopicTitle("Konu 18");
        beginner.setTopicDescription("Preferences, Dialog Ekranları, Widgetlar");
        beginner.setTopicDetail("a.\tPreferences\n" +
                "b.\tDialog Ekranları\n" +
                "c.\tWidgetlar\n");
        beginners.add(beginner);


        String pdfPath = "A.Konu";

        for (int i = 0; i <= 17; i++) {
            beginners.get(i).setPath(pdfPath + (i + 1) + ".pdf");}

        return beginners;
    }
}
