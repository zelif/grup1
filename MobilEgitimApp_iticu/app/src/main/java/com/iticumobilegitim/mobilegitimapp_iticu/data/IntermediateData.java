package com.iticumobilegitim.mobilegitimapp_iticu.data;

import com.iticumobilegitim.mobilegitimapp_iticu.model.Intermediate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saliha on 2.6.2016.
 */
public class IntermediateData {

    public static List<Intermediate> getIntermediateTopics() {
        List<Intermediate> intermediates = new ArrayList<>();

        Intermediate intermediate = new Intermediate();
        intermediate.setId(1);
        intermediate.setTopicTitle("Konu 1");
        intermediate.setTopicDescription("Genel Java Dili");
        intermediate.setTopicDetail("a.\tProgramlama Nedir?\n" +
                "b.\tDerleyici ve Yorumlayıcı Kavramları\n" +
                "c.\tJava Programlama Dilinin Özellikleri\n" +
                "d.\tJava Programlama Dilinin Yapısı\n" +
                "e.\tJava Programlama Dilinin Kalıbı\n" +
                "f.\tAlgoritma Nedir?\t\n" +
                "g.\tProgramlama Dillerinin Temel Yapı Taşları\n" +
                "h.\tDeğişkenler\n" +
                "i.\tVeri Tipleri\n" +
                "j.\tOperatörler \n");
        intermediates.add(intermediate);

        intermediate = new Intermediate();
        intermediate.setId(2);
        intermediate.setTopicTitle("Konu 2");
        intermediate.setTopicDescription("Nesne Yönelimli Programlama ve Temel Kavramlar");
        intermediate.setTopicDetail("a.\tUML Diyagramları\n" +
                "b.\tNesne Tabanlı Tasarım\n" +
                "c.\tFonksiyonlar (Methodlar)\n" +
                "d.\tKalıtım (Inheritence) Mantığı\n" +
                "e.\tÜzerine Yazma (Method Overriding)\n" +
                "f.\tSarma (Encapsulation)\n" +
                "g.\tÇok Biçimlilik (Polimorfizm)\n");
        intermediates.add(intermediate);


        intermediate = new Intermediate();
        intermediate.setId(3);
        intermediate.setTopicTitle("Konu 3");
        intermediate.setTopicDescription("Android'e Giriş");
        intermediate.setTopicDetail("a.\tAndroid Kavramı\n" +
                "b.\tAndroid Geliştirme Araçları\n" +
                "c.\tAndroid Geliştirme Ortamı Kurulumu\n" +
                "d.\tAndroid SDK Yöneticisi\n" +
                "e.\tEmulatör Kullanımı\n" +
                "f.\tUygulama Temelleri\n" +
                "g.\tCihaz Uyumlulukları\n" +
                "h.\tSistem İzinleri\n" +
                "i.\tAndroid Studio Hakkında\n");
        intermediates.add(intermediate);

        intermediate = new Intermediate();
        intermediate.setId(4);
        intermediate.setTopicTitle("Konu 4");
        intermediate.setTopicDescription("Uygulama Elemanları, Intentler, Aktiviteler, Fragmentlar");
        intermediate.setTopicDetail("a.\tIntent ve Intent Filtreleri\n" +
                "i.\tYaygın İntent’ler\n" +
                "b.\tAktiviteler\n" +
                "i.\tAktivite Oluşturmak\n" +
                "ii.\tSonuç Beklenen Aktivite Başlatmak\n" +
                "iii.\tAktivite Hayat Döngüsü\n" +
                "iv.\tYeni Aktiviteye Veri Aktarımı\t\n" +
                "v.\tFragmentlar\n");
        intermediates.add(intermediate);

        intermediate = new Intermediate();
        intermediate.setId(5);
        intermediate.setTopicTitle("Konu 5");
        intermediate.setTopicDescription("Fragmentların Yönetilmesi, Servisler, İçerik Sağlayıcılar, İşlemler ve Threadler");
        intermediate.setTopicDetail("a.\tFragmentların Yönetilmesi \n" +
                "b.\tGörevler ve Uygulama Yığını\n" +
                "c.\tServisler\n" +
                "d.\tİçerik Sağlayıcılar\n" +
                "e.\tİşlemler ve Thread’ler\n");
        intermediates.add(intermediate);

        intermediate = new Intermediate();
        intermediate.setId(6);
        intermediate.setTopicTitle("Konu 6");
        intermediate.setTopicDescription("Android Debug Bridge(adb) Aracı ve .apk Dosyası Çıkarma");
        intermediate.setTopicDetail("a.\tUygulama Hata Ayıklama\n" +
                "b.\tUygulama Log Yazdırma\n" +
                "c.\tUygulamanın .Apk Dosyasının Çıkarılması\n");
        intermediates.add(intermediate);

        intermediate = new Intermediate();
        intermediate.setId(7);
        intermediate.setTopicTitle("Konu 7");
        intermediate.setTopicDescription("Kullanıcı Arayüzleri");
        intermediate.setTopicDetail("a.\tPreferences\n" +
                "b.\tDialog Ekranları\n" +
                "c.\tWidgetlar\n");
        intermediates.add(intermediate);

        intermediate = new Intermediate();
        intermediate.setId(8);
        intermediate.setTopicTitle("Konu 8");
        intermediate.setTopicDescription("Fragment Kullanımı, ActionBar, Adaptörler ve Widgetlar");
        intermediate.setTopicDetail("a.\tFragment Hayat Döngüsü\n" +
                "b.\tAction Bar\n" +
                "c.\tAdaptörler\n");
        intermediates.add(intermediate);

        intermediate = new Intermediate();
        intermediate.setId(9);
        intermediate.setTopicTitle("Konu 9");
        intermediate.setTopicDescription("Web Servisler, İçerik Sağlayıcılar, Dosya Yönetimi");
        intermediate.setTopicDetail("a.\tVolley\n" +
                "b.\tSQLite\n" +
                "c.\tDosya Yönetimi\n");
        intermediates.add(intermediate);

        intermediate = new Intermediate();
        intermediate.setId(10);
        intermediate.setTopicTitle("Konu 10");
        intermediate.setTopicDescription("Broadcast Receiver ve Servisler");
        intermediate.setTopicDetail("a.\tBroadcast Receiver\n" +
                "b.\tServisler\n");
        intermediates.add(intermediate);

        intermediate = new Intermediate();
        intermediate.setId(11);
        intermediate.setTopicTitle("Konu 11");
        intermediate.setTopicDescription("Konum ve Sensörler");
        intermediate.setTopicDetail("");
        intermediates.add(intermediate);

        intermediate = new Intermediate();
        intermediate.setId(12);
        intermediate.setTopicTitle("Konu 12");
        intermediate.setTopicDescription("Medya, Kamera, Animasyon ve Grafikler");
        intermediate.setTopicDetail("");
        intermediates.add(intermediate);

        intermediate = new Intermediate();
        intermediate.setId(13);
        intermediate.setTopicTitle("Konu 13");
        intermediate.setTopicDescription("Linkfy");
        intermediate.setTopicDetail("");
        intermediates.add(intermediate);

        intermediate = new Intermediate();
        intermediate.setId(14);
        intermediate.setTopicTitle("Konu 14");
        intermediate.setTopicDescription("Uygulama Kaynakları");
        intermediate.setTopicDetail("a.\tTemel Bilgiler\n" +
                "b.\tKaynakların Sağlanması\n" +
                "c.\tKaynaklara Erişim\n" +
                "d.\tKaynakların Çalışma Zamanında Güncellenmesi\n" +
                "e.\tYerelleştirme\n" +
                "f.\tKaynak Tipleri\n" +
                "i.\tAnimasyon\n" +
                "ii.\tRenk Durum Listesi\n" +
                "iii.\tÇizilebilir\n" +
                "iv.\tDüzen\n" +
                "v.\tMenü\n" +
                "vi.\tString\n" +
                "vii.\tStil\n");
        intermediates.add(intermediate);

        String pdfPath = "B.Konu";

        for (int i = 0; i <= 13; i++) {
            intermediates.get(i).setPath(pdfPath + (i + 1) + ".pdf");}
        return intermediates;
}}
