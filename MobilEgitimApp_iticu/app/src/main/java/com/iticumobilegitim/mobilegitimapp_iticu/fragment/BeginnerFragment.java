package com.iticumobilegitim.mobilegitimapp_iticu.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iticumobilegitim.mobilegitimapp_iticu.DividerItemDecoration;
import com.iticumobilegitim.mobilegitimapp_iticu.R;
import com.iticumobilegitim.mobilegitimapp_iticu.activity.DetailActivityForBeginner;
import com.iticumobilegitim.mobilegitimapp_iticu.adapter.BeginnerAdapter;
import com.iticumobilegitim.mobilegitimapp_iticu.data.BeginnerData;
import com.iticumobilegitim.mobilegitimapp_iticu.listener.ItemClickCallback;
import com.iticumobilegitim.mobilegitimapp_iticu.model.Beginner;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeginnerFragment extends Fragment implements ItemClickCallback {

    private RecyclerView mRecyclerView;
    private BeginnerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Beginner> mBeginner;
    private View mRootView;

    private static final String BUNDLE_EXTRAS = "BUNDLE_EXTRAS";
    private static final String DETAIL_BEGINNER = "DETAIL_BEGINNER";
    private static final String TITLE_BEGINNER = "TITLE_BEGINNER";
    private static final String DESCRIPTION_BEGINNER = "DESCRIPTION_BEGINNER";
    private static final String PATH_BEGINNER = "PATH_BEGINNER";


    public BeginnerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_beginner, container, false);

        mBeginner = BeginnerData.getBeginnerTopics();
        mAdapter = new BeginnerAdapter(this, mBeginner, getContext());
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.beginner_recyclerview);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);

        return mRootView;
    }

    @Override
    public void onItemClick(int p) {

        String topicTitle = mBeginner.get(p).getTopicTitle();
        String topicDetail = mBeginner.get(p).getTopicDetail();
        String topicDescription = mBeginner.get(p).getTopicDescription();
        String path = mBeginner.get(p).getPath();

        Intent intent = new Intent(getActivity(), DetailActivityForBeginner.class);

        Bundle extras = new Bundle();
        extras.putString(DETAIL_BEGINNER, topicDetail);
        extras.putString(TITLE_BEGINNER, topicTitle);
        extras.putString(DESCRIPTION_BEGINNER,topicDescription);
        extras.putString(PATH_BEGINNER,path);
        intent.putExtra(BUNDLE_EXTRAS, extras);
        getActivity().startActivity(intent);


 /*       Fragment newFragment = new TopicDetailFragment();

        Bundle extras = new Bundle();
        extras.putString(DETAIL_BEGINNER, topicDetail);
        newFragment.setArguments(extras);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frm_beginer, newFragment);
        ft.addToBackStack(null);
        ft.commit();*/


    }

}
