package com.iticumobilegitim.mobilegitimapp_iticu.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iticumobilegitim.mobilegitimapp_iticu.DividerItemDecoration;
import com.iticumobilegitim.mobilegitimapp_iticu.R;
import com.iticumobilegitim.mobilegitimapp_iticu.activity.DetailActivityForIntermediate;
import com.iticumobilegitim.mobilegitimapp_iticu.adapter.IntermediateAdapter;
import com.iticumobilegitim.mobilegitimapp_iticu.data.IntermediateData;
import com.iticumobilegitim.mobilegitimapp_iticu.listener.ItemClickCallback;
import com.iticumobilegitim.mobilegitimapp_iticu.model.Intermediate;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntermediateFragment extends Fragment implements ItemClickCallback{

    private RecyclerView mRecyclerView;
    private IntermediateAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Intermediate> mIntermediate;
    private View mRootView;

    private static final String BUNDLE_EXTRAS = "BUNDLE_EXTRAS";
    private static final String TITLE_INTERMEDIATE = "TITLE_INTERMEDIATE";
    private static final String DETAIL_INTERMEDIATE = "DETAIL_INTERMEDIATE";
    private static final String DESCRIPTION_INTERMEDIATE = "DESCRIPTION_INTERMEDIATE";
    private static final String PATH_INTERMEDIATE = "PATH_INTERMEDIATE";

    public IntermediateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_intermediate, container, false);

        mIntermediate = IntermediateData.getIntermediateTopics();
        mAdapter = new IntermediateAdapter(this,mIntermediate,getContext());
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.intermediate_recyclerview);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);

        return mRootView;
    }


    @Override
    public void onItemClick(int p) {

        String topicDetail =  mIntermediate.get(p).getTopicDetail();
        String topicTitle =  mIntermediate.get(p).getTopicTitle();
        String topicDescription =  mIntermediate.get(p).getTopicDescription();
        String path = mIntermediate.get(p).getPath();

        Intent intent = new Intent(getActivity(),DetailActivityForIntermediate.class);

        Bundle extras = new Bundle();
        extras.putString(DETAIL_INTERMEDIATE, topicDetail);
        extras.putString(TITLE_INTERMEDIATE,topicTitle);
        extras.putString(DESCRIPTION_INTERMEDIATE,topicDescription);
        extras.putString(PATH_INTERMEDIATE,path);
        intent.putExtra(BUNDLE_EXTRAS,extras);
        getActivity().startActivity(intent);


    }
}
