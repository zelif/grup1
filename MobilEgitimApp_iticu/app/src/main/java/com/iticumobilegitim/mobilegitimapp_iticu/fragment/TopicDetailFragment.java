package com.iticumobilegitim.mobilegitimapp_iticu.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iticumobilegitim.mobilegitimapp_iticu.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopicDetailFragment extends Fragment {

    private static final String DETAIL_INTERMEDIATE = "DETAIL_INTERMEDIATE";
    private static final String DETAIL_BEGINNER = "DETAIL_BEGINNER";

    private String topicDescription;


    public TopicDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_topic_detail, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            topicDescription = bundle.getString(DETAIL_BEGINNER, "topicDescription");

        }
        TextView textView = (TextView) getActivity().findViewById(R.id.tv_dene);
        textView.setText(topicDescription);
    }
}
