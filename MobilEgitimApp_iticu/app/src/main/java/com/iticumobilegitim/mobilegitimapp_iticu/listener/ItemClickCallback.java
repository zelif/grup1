package com.iticumobilegitim.mobilegitimapp_iticu.listener;

/**
 * Created by User on 9.6.2016.
 */
public interface ItemClickCallback {

    void onItemClick(int p);
}
