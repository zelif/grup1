package com.iticumobilegitim.mobilegitimapp_iticu.model;

/**
 * Created by saliha on 4.6.2016.
 */
public class About {

    private int id;
    protected String aboutText;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAboutText() {
        return aboutText;
    }

    public void setAboutText(String aboutText) {
        this.aboutText = aboutText;
    }
}
