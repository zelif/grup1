package com.iticumobilegitim.mobilegitimapp_iticu.model;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.os.Environment;

import com.iticumobilegitim.mobilegitimapp_iticu.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Secur {

    static final String TAG = "SECURR";

    public static void decode(Context context,InputStream fisPdf, String pdfFilePath ) {
        System.out.println("decodee basladi");
//        String defName = "decrypted";
//        String path = null;
        String ext = ".pdf";

        try {
            File wwRoot = new File(context.getFilesDir()+"/decrypted.pdf");

//            path = wwRoot + "/" + defName + ext;
//            System.out.println(new File(path).exists() + "yoll:" + path);
//            if (!(new File(path).exists())) {
            String str;
            InputStream fis = fisPdf;
            FileOutputStream fos = new FileOutputStream(wwRoot);
            //daha onceden varsa tekrar kopyalama diye
            str = context.getResources().getString(R.string.ali)
                    + pdfFilePath
                    + context.getResources().getString(R.string.veli);

            byte[] myKeyz = str.getBytes(context.getResources().getString(R.string.utf));
            SecretKeySpec sks = new SecretKeySpec(getKey(myKeyz), context.getResources().getString(R.string.aes));
            Cipher cipher = Cipher.getInstance(context.getResources().getString(R.string.cipher));

            byte[] aByte = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            IvParameterSpec ivSpec = new IvParameterSpec(aByte);
            cipher.init(Cipher.DECRYPT_MODE, sks, ivSpec);
            CipherInputStream cis = new CipherInputStream(fis, cipher);

            System.out.println("decodee, kayit badladi");
            int b;
            byte[] d = new byte[8192];//buffer size veri aktarım hızını doğrudan etkilediği için yüksek bir rakam verdik,
            while ((b = cis.read(d)) != -1) {
                fos.write(d, 0, b);
            }
            fos.flush();
            fos.close();
            cis.close();
//            }
            System.out.println("decodee bitti");

        } catch (Exception e) {

            System.out.println("decodee hata aldik");
            e.printStackTrace();
        }
    }

    public static String encode(Context context,InputStream fisPdf, String pdfFilePath ) {
        System.out.println("encode basladi");
        //  String defName = "decrypted";
        String path = null;
        String ext = ".pdf";

        try {
            File wwRoot = new File(context.getDir(context.getResources().getString(R.string.reader), Context.MODE_PRIVATE).getAbsolutePath() + "/"
                    + context.getResources().getString(R.string.reader) + "/" + context.getResources().getString(R.string.books_folder));

            path = wwRoot + "/" + pdfFilePath;
            System.out.println(new File(path).exists() + "yoll:" + path);
//            if (!(new File(path).exists())) {
            String str;

            InputStream fis = fisPdf;
            File mFile = new File(Environment.getExternalStorageDirectory().toString() + "/iticu/", pdfFilePath);
            mFile.setReadable(true, false);
            FileOutputStream fos = new FileOutputStream(mFile);
//            FileOutputStream fos = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/iticu/"+pdfFilePath);

            //daha onceden varsa tekrar kopyalama diye
            str = context.getResources().getString(R.string.ali)
                    + pdfFilePath
                    + context.getResources().getString(R.string.veli);

            byte[] myKeyz = str.getBytes(context.getResources().getString(R.string.utf));
            SecretKeySpec sks = new SecretKeySpec(getKey(myKeyz), context.getResources().getString(R.string.aes));
            Cipher cipher = Cipher.getInstance(context.getResources().getString(R.string.cipher));

            byte[] aByte = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            IvParameterSpec ivSpec = new IvParameterSpec(aByte);
            cipher.init(Cipher.ENCRYPT_MODE, sks, ivSpec);
            CipherInputStream cis = new CipherInputStream(fis, cipher);

            System.out.println("decodee, kayit badladi");
            int b;
            byte[] d = new byte[8192];//buffer size veri aktarım hızını doğrudan etkilediği için yüksek bir rakam verdik,
            while ((b = cis.read(d)) != -1) {
                fos.write(d, 0, b);
            }
            fos.flush();
            fos.close();
            cis.close();
//            }
            System.out.println("decodee bitti");

        } catch (Exception e) {

            System.out.println("decodee hata aldik");
            e.printStackTrace();
            return null;
        }

        return path;
    }

    private static byte[] getKey(byte[] suggestedKey) {
        byte[] kRaw = suggestedKey;
        byte[] result = new byte[16];
        int j = 0;
        for (int i = 0; i < 128; i += 8) {
            result[j] = kRaw[(i / 8) % kRaw.length];
            j++;
        }
        return result;
    }
}
